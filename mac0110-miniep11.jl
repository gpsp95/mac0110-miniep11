using Unicode
function palindromo(texto)
    txt_tratado = ""
    if length(texto) == 0 || length(texto) == 1
        return true
    end
    for i in lowercase(Unicode.normalize(texto, stripmark=true))
        if islowercase(i)
            txt_tratado *= i
        end
    end
    
    ponteiro1 = 1
    ponteiro2 = length(txt_tratado)
    
    while ponteiro1 <= ponteiro2
        if txt_tratado[ponteiro1] != txt_tratado[ponteiro2]
            return false
        end
        
        ponteiro1 += 1
        ponteiro2 -= 1
    end
    return true
end

using Test
function test()
    @test palindromo("ovo")    
    @test palindromo("") 
    @test palindromo("Hannah") 
    @test palindromo(" ") 
    @test palindromo("B") 
    @test palindromo("A mãe te ama!") 
    @test palindromo("Socorram-me, subi no ônibus em Marrocos!") 
        
    @test !palindromo("Bolsonaro pegou covid") 
    @test !palindromo("Poxa... que pena")
    @test !palindromo("Não é qualquer frase que é um palindromo")
   
end
test()
